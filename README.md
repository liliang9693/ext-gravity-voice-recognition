# Gravity 语音识别&合成模块


![](./python/_images/featured.png)

---------------------------------------------------------

## Table of Contents

* [URL](#url)
* [Summary](#summary)
* [Blocks](#blocks)
* [License](#license)
* [Supported targets](#Supportedtargets)

## URL
* Project URL : ```https://gitee.com/liliang9693/ext-gravity-voice-recognition```

* Tutorial URL : ```https://mindplus.dfrobot.com.cn/extensions-user```

* 商品链接: ```https://www.dfrobot.com.cn/goods-3673.html```



## Summary
此扩展支持DFRobot出品的SEN0539：Gravity:语音识别合成模块


## Blocks

![](./python/_images/blocks.png)



## Examples
### 上传模式

![](./arduinoC/_images/example.png) 

### Python模式行空板


![](./python/_images/example1.png)  

![](./python/_images/example2.png)  

## License

MIT

## Supported targets

MCU                | JavaScript    | Arduino   | MicroPython    | unihiker
------------------ | :----------: | :----------: | :---------: | -----
arduino        |             |        √      |             | 
micro:bit V1       |             |       √       |             | 
micro:bit V2      |             |       √       |             | 
esp32        |             |        √      |             | 
python        |             |              |             |  √

## Release Logs
* V0.0.1  基础功能完成
* V0.0.2  屏蔽arduino库中串口代码
* V0.0.3  更新库文件，声音范围调整为1-7,添加读取None的处理代码
* V0.0.4  增加行空板用户库路径，解决模块不存在问题
* V0.0.5  Python模式识别增加等待，防止识别过快出错


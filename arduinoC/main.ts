
//% color="#1296db" iconWidth=50 iconHeight=40
namespace GravityVoiceRecognition {

    //% block="Voice Recognition setup I2C mode address 0x64" blockType="command"
    export function init(parameter: any, block: any) {

 
        Generator.addInclude("DFRobot_asrDF2301Q", `#include "DFRobot_DF2301Q.h"`);
        Generator.addObject("asrDF2301Q",`DFRobot_DF2301Q_I2C`,`asrDF2301Q;`);
        Generator.addCode(`while (!(asrDF2301Q.begin())) {delay(3000);}`);
 
    }

       //% block="---"
       export function noteSep() {

       }
    
    //% block="set volume [VOL]" blockType="command"
    //% VOL.shadow="range"   VOL.params.min=1    VOL.params.max=7    VOL.defl=5
    export function setVolume(parameter: any, block: any) {
        let vol=parameter.VOL.code;

        Generator.addCode(`asrDF2301Q.setVolume(${vol});`);
 
    }
  
    //% block="set mute mode [MUTE]" blockType="command"
    //% MUTE.shadow="dropdown"   MUTE.options="MUTE"
    export function setMuteMode(parameter: any, block: any) {
        let mute=parameter.MUTE.code;

        Generator.addCode(`asrDF2301Q.setMuteMode(${mute});`);
 
    }
    //% block="set wake time[WT]" blockType="command"
    //% WT.shadow="range"   WT.params.min=0    WT.params.max=255    WT.defl=20
    export function setWakeTime(parameter: any, block: any) {
        let wt=parameter.WT.code;

        Generator.addCode(`asrDF2301Q.setWakeTime(${wt});`);
 
    }
    //% block="get wake time" blockType="reporter"
    export function getWakeTime(parameter: any, block: any) {
        Generator.addCode(`asrDF2301Q.getWakeTime()`);
 
    }

        //% block="---"
        export function noteSep1() {

        }

    //% block="play [ID]" blockType="command"
    //% ID.shadow="number"   ID.defl=23
    export function playByCMDID(parameter: any, block: any) {
        let id=parameter.ID.code;

        Generator.addCode(`asrDF2301Q.playByCMDID(${id});`);
 
    }
    
        //% block="---"
        export function noteSep2() {

        }
    //% block="identify once and save the results" blockType="command"
    export function getCMDID(parameter: any, block: any) {
        Generator.addCode(`int asrDF2301Q_CMDID = asrDF2301Q.getCMDID();`);
 
    }
    
    //% block="recognize it?" blockType="boolean"
    export function checkCMDID(parameter: any, block: any) {
        Generator.addCode(["not asrDF2301Q_CMDID==0", Generator.ORDER_UNARY_POSTFIX]);
 
    }
    //% block="get the result" blockType="reporter"
    export function readCMDID(parameter: any, block: any) {
        Generator.addCode(["asrDF2301Q_CMDID", Generator.ORDER_UNARY_POSTFIX]);
 
 
    }
     
        //% block="---"
        export function noteSep3() {

        }
    //% block="word [WORD1] ID" blockType="reporter"
    //% WORD1.shadow="dropdown"   WORD1.options="WORD1"
    export function checkword1(parameter: any, block: any) {
        let word=parameter.WORD1.code;
        Generator.addCode(`${word}`);
 
    }
    //% block="word [WORD2] ID" blockType="reporter"
    //% WORD2.shadow="dropdown"   WORD2.options="WORD2"
    export function checkword2(parameter: any, block: any) {
        let word=parameter.WORD2.code;
        Generator.addCode(`${word}`);
 
    }
    //% block="word [WORD3] ID" blockType="reporter"
    //% WORD3.shadow="dropdown"   WORD3.options="WORD3"
    export function checkword3(parameter: any, block: any) {
        let word=parameter.WORD3.code;
        Generator.addCode(`${word}`);
    }
    //% block="word [WORD4] ID" blockType="reporter"
    //% WORD4.shadow="dropdown"   WORD4.options="WORD4"
    export function checkword4(parameter: any, block: any) {
        let word=parameter.WORD4.code;
        Generator.addCode(`${word}`);
 
    
    }

}
